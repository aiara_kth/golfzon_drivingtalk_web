
//메뉴를 가져와서 접속한 페이지를 Actice
$(document).ready(function(){

    let item = $("nav[data-spy=appy-menu]");
    let footer = $('footer');
    let select = item.attr("data-pagename");

    item.load('header.html');
    footer.load('footer.html');

    //hover설정
    setTimeout(function(){
        if(!$('#main_menu').children().children('li[data-navmenu='+select+']').hasClass('active')){
            $('#main_menu').children().children('li[data-navmenu='+select+']').addClass('active');
            console.log('add active');
        } else{
            console.log('already have active');
        }
    }, 100);


    //슬라이더 버튼 숨김처리
    $('.slider_block_go_left').ready(function(){
        $('.slider_block_go_left').parent().hide();
    });
    $('.slider_block_go_right').ready(function(){
        $('.slider_block_go_right').parent().hide();
    });
});



//선택에서 동영상 다운로드
$('.video_dropbox').on('change',function(){
    let videe_download_url = $(this).attr('data-baseurl');
    let video_filename = $('option:selected',this).attr('data-filename');
    if(video_filename == 'none'){
        return false;
    }
    console.log(video_filename);
    console.log('download_video');
    $('<a/>',{
        "href": videe_download_url + video_filename,
        "download":"bigbuckbunny.mov",
        id:"video_download_a_tag"
    }).appendTo(document.body);
    $('#video_download_a_tag').get(0).click();
    $('#video_download_a_tag').remove();
    $(this).val('none');
    return true;

});

//링크 이동
function go_url(url,type){
    switch(type){
        case 'facebook':
            location.href = 'https://www.facebook.com/' + url; break;
        case 'youtube':
            location.href = 'https://youtube.com/' + url; break;
        case 'twitter':
            location.href = 'https://twitter.com/' + url; break;
        case 'naverblog':
            location.href = 'http://blog.naver.com/' + url;  break;
        case 'navercafe':
            location.href = 'http://cafe.naver.com/' + url;  break;
        case 'tistory':
            location.href = 'http://' + url + '.tistory.com/';  break;
        default:
            location.href = url; break;
    }
}

//슬라이더 설정
$('.ff-slider').on('translate.owl.carousel', function(e){
    idx = e.item.index;
    console.log(idx);
    $('.slider_block_big').addClass('slider_block');
    $('.slider_block_big').removeClass('slider_block_big');
    $('.ff_slider').eq(idx).addClass('slider_block_big');
    $('.ff_slider').eq(idx).removeClass('slider_block');
});